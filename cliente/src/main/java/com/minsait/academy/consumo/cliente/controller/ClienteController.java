package com.minsait.academy.consumo.cliente.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.minsait.academy.consumo.cliente.entity.ClienteEntity;
import com.minsait.academy.consumo.cliente.service.ClienteService;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/libreria")
public class ClienteController {

	private final RestTemplate restTemplate;

	@Autowired
	public ClienteController(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Autowired
	private ClienteService clienteService;

	@GetMapping("/books")
	public List<ClienteEntity> listar() {
		log.info("Extraccion de todos los libros");
		return clienteService.findAll();
	}

	@GetMapping("/books/{id}")
	public ClienteEntity get(@PathVariable Long id) {
		log.info("Extraccion de libro en posicion id =" + id);
		return clienteService.findById(id);
	}

	@PostMapping(path = "/", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ClienteEntity> createLibro(@RequestBody ClienteEntity libro) {
		log.info("Libro guardado");
		return restTemplate.postForEntity("https://9ce6-2806-2a0-400-8a4e-4803-f374-6ffa-9b48.ngrok.io/libreria/books/",
				libro, ClienteEntity.class);
	}

}
