package com.minsait.academy.consumo.cliente.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.minsait.academy.consumo.cliente.entity.ClienteEntity;


@Service
public class ClienteService {

	private final RestTemplate restTemplate;

	@Autowired
	public ClienteService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	public List<ClienteEntity> findAll() {
		String url = "https://9ce6-2806-2a0-400-8a4e-4803-f374-6ffa-9b48.ngrok.io/libreria/books/";
		ClienteEntity[] resultado = restTemplate.getForObject(url, ClienteEntity[].class);
		return Arrays.asList(resultado);
	}

	public ClienteEntity findById(Long id) {
		Map<String, String> pathVariables = new HashMap<String, String>();
		pathVariables.put("id", id.toString());
		String url = "https://9ce6-2806-2a0-400-8a4e-4803-f374-6ffa-9b48.ngrok.io/libreria/books/{id}";
		ClienteEntity cliente = restTemplate.getForObject(url, ClienteEntity.class, pathVariables);
		return cliente;
	}
	

}
